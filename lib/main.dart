import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Container Example',
        home: Scaffold(
          body: SafeArea(
              child: Container(
                color: Colors.black,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  color: Colors.white,
                  width: 100,
                  height: 100,
                ),
                Container(
                  color: Colors.black,
                  width: 100,
                  height: 100,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        color: Colors.deepOrange,
                        width: 100,
                        height: 100,
                      ),
                    Container(
                      color: Colors.yellowAccent,
                      width: 100,
                      height: 100,)],
                  ),
                ),
                Container(
                  color: Colors.blue,
                  width: 100,
                  height: 100,
                )
              ],
            ),
          )),
        ));
  }
}
